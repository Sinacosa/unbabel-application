//
//  NetworkLayer.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// Mark: - Networking funcs
struct NetworkLayer: Requestable {
  
  enum RequestResponse {
    case success(json: JSON)
    case failure
  }
  
  enum Endpoint {
    case postList
    case postComment(Int)
    case user(Int)
    
    static private let baseURL = "http://jsonplaceholder.typicode.com/"
    
    func getUrl () -> URL? {
      switch self {
      case .postList:
        return URL(string: Endpoint.baseURL + "posts/")
      case let .postComment(id):
        return URL(string: Endpoint.baseURL + "posts/" + String(id) + "/comments")
      case let .user(id):
        return URL(string: Endpoint.baseURL + "users/" + String(id))
      }
    }
  }
  
  private struct Request {
    static func run (url: URL, method: HTTPMethod, parameters: Parameters?, completion: @escaping ( _ res: RequestResponse) -> Void) {
      Alamofire.request(url, method: method, parameters: parameters).response { (response) in
        guard let responseData = response.data,
              let statusCode = response.response?.statusCode,
              (200...399).contains(statusCode)
        else {
          completion(.failure)
          return
        }
        do {
          try completion(.success(json: JSON(data: responseData)))
        } catch {
          completion(.failure)
        }
      }
    }
  }
}

// Mark:- Post request
extension NetworkLayer {
  
  static func getAllPosts(completion: @escaping((Bool, [Post]? ) -> Void)) {
    guard let url = Endpoint.postList.getUrl() else { completion(false, nil); return }
    Request.run(url: url, method: .get, parameters: nil) { (response) in
      switch response {
      case let .success(jsonData):
        completion(true, jsonData.arrayValue.map { json in Post(json: json) })
      case .failure:
        completion(false, nil)
      }
    }
  }
}
// Mark: - Get User and Comments for a given post
extension NetworkLayer {
  
  static func getUserAndComments(for post: Post, completion: @escaping((Bool, User?, [Comment]?) -> Void)) {
    let url_comments = Endpoint.postComment(post.id).getUrl()!
    let url_user = Endpoint.user(post.userId).getUrl()!

    DispatchQueue.global(qos: .utility).async {
      let dispatchGroup = DispatchGroup()
      var success = true
      var commentJson: JSON?
      var userJson: JSON?
      
      dispatchGroup.enter()
      getUser(url: url_user, completion: { (successResult, json) in
        success = success && successResult
        userJson = json
        dispatchGroup.leave()
      })
      
      dispatchGroup.enter()
      getComments(url: url_comments, completion: { (successResult, json) in
        commentJson = json
        success = success && successResult
        dispatchGroup.leave()
      })
      
      dispatchGroup.notify(queue: DispatchQueue.main, execute: {
        guard let userJson = userJson, let commentJson = commentJson else {
          completion(false, nil, [])
          return
        }
        let comments = commentJson.arrayValue.map{ Comment.init(json: $0) }
        completion(success, User.init(json: userJson), comments)
      })
    }
  }
  
  private static func getUser(url: URL, completion: @escaping((Bool, JSON?) -> Void)) {
    Request.run(url: url, method: .get, parameters: nil, completion: { response in
      switch response {
      case let .success(json):
        completion(true, json)
      case .failure:
        completion(false, nil)
      }
    })
  }
  
  private static func getComments(url: URL, completion: @escaping((Bool, JSON?) -> Void)) {
    Request.run(url: url, method: .get, parameters: nil, completion: { response in
      switch response {
      case let .success(json):
        completion(true, json)
      case .failure:
        completion(false, nil)
      }
    })
  }
}

protocol Requestable {
  static func getAllPosts(completion: @escaping((Bool, [Post]? ) -> Void))
  static func getUserAndComments(for post: Post, completion: @escaping((Bool, User?, [Comment]?) -> Void))
}
