//
//  UINavigationControllerExtension.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 24/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationBar {
  
  func setTheme() {
    isTranslucent = false
    shadowImage = UIImage()
    layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
    layer.shadowOffset = CGSize(width: 0, height: 0)
    layer.shadowOpacity = 1.0
    layer.shadowRadius = 20.0
    layer.masksToBounds = false
    largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: Theme.Colors.darkBlue]
  }
}
