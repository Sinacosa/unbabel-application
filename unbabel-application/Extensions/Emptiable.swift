//
//  Emptiable.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 25/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import UIKit

protocol Emptiable {
  var emptyView: EmptyView! { get set }
}

extension Emptiable {
  func setEmptyView(in view: UIView) {
    view.insertSubview(emptyView, at: 0)
    emptyView.translatesAutoresizingMaskIntoConstraints = false
    emptyView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
    emptyView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    emptyView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0).isActive = true
    emptyView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0).isActive = true
  }
  
  func removeEmptyView() {
    emptyView.removeFromSuperview()
  }
}
