//
//  Refreshable.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 24/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import UIKit

@objc protocol Refreshable: class where Self: UIViewController {
  var collectionView: UICollectionView! { get set }
  @objc func handleRefresControl ()
}

extension Refreshable {
  
  func setRefreshControl () {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(handleRefresControl), for: .valueChanged)
    collectionView.refreshControl = refreshControl
    refreshControl.layer.zPosition = -1
  }
  
  func stopRefreshing () {
    collectionView.refreshControl?.endRefreshing()
  }
}
