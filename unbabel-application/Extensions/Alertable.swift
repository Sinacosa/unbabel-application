//
//  File.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 24/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import UIKit

protocol Alertable: class where Self: UIViewController { }

extension Alertable {
  func showAlert(title:String, message: String, actions: UIAlertAction...) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    actions.forEach { alertController.addAction($0) }
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      self.present(alertController, animated: true, completion: nil)
    }
  }
}
