//
//  Theme.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import UIKit

struct Theme {
  struct Colors {
    static let darkBlue = UIColor(red: 38/255, green: 51/255, blue: 70/255, alpha: 1)
    static let lightGray = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
  }
  struct Font {
    static let title = UIFont(name: "Avenir-Medium", size: 15)!
    static let medium = UIFont(name: "Avenir-Book", size: 13)!
    static let small = UIFont(name: "Avenir-Book", size: 11)!
    static let largeTitle = UIFont(name: "Avenir-Heavy", size: 20)!
  }
}
