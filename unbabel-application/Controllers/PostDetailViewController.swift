//
//  PostDetailViewController.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

fileprivate let postDetailCellIdentifier = "postDetailCellIdentifier"
fileprivate let commentDetailCellIdentifier = "commentDetailCellIdentifier"
fileprivate let sectionTitleIdentifier = "sectionTitleIdentifier"

class PostDetailViewController: UIViewController, Alertable, Refreshable {
  
  @IBOutlet weak var collectionView: UICollectionView!
  var viewModel: PostDetailViewModel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setRefreshControl()
    title = "Post Detail"
    setUI()
    bind()
    viewModel.fetch()
  }
  
  private func bind() {
    viewModel.onError = { [weak self] in
      let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
      self?.showAlert(title: "Oops Someting went wrong...",
                      message: "Try to reload, or come back later.",
                      actions: alertAction)
      self?.stopRefreshing()
    }
    viewModel.onFetched = { [weak self] in
      self?.collectionView.reloadData()
      self?.stopRefreshing()
    }
  }
  
  private func setUI() {
    collectionView.register(DetailTitleView.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: sectionTitleIdentifier)
    collectionView.contentInset.top = 20
    collectionView.contentInset.bottom = 20
    navigationItem.largeTitleDisplayMode = .never
    view.backgroundColor = Theme.Colors.lightGray
  }
  
  internal func handleRefresControl() {
    viewModel.fetch()
  }
}

// Mark: - CollectionView DataSource, Delegate and DelegateFlowLayout methods
extension PostDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 2
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return section == 0 ? 1 : viewModel.comments.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.section == 0 {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postDetailCellIdentifier, for: indexPath) as! PostDetailCollectionViewCell
      cell.configure(post: viewModel.post, user: viewModel.user)
      return cell
    } else {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: commentDetailCellIdentifier, for: indexPath) as! CommentCollectionViewCell
      cell.configure(comment: viewModel.comments[indexPath.item])
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if indexPath.section != 0 {
      return CommentCollectionViewCell.getHeight(width: collectionView.bounds.width - 40, comment: viewModel.comments[indexPath.item])
    }
    return PostDetailCollectionViewCell.getHeight(width: collectionView.bounds.width - 40, post: viewModel.post)
  }
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String,
                      at indexPath: IndexPath) -> UICollectionReusableView {
    switch kind {
    case UICollectionElementKindSectionHeader:
      let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: sectionTitleIdentifier,
                                                                       for: indexPath) as! DetailTitleView
      headerView.titleLabel.text = "COMMENTS(\(viewModel.comments.count))"
      return headerView
    default:
      assert(false, "\(kind) not supported")
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                      referenceSizeForHeaderInSection section: Int) -> CGSize {
    return CGSize(width: collectionView.bounds.width, height: section != 0 ? 40 : 0)
  }
}
