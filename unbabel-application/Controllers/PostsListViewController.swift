//
//  ViewController.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

fileprivate let postCellIdentifier = "postCellIdentifier"

protocol PostsListViewControllerDelegate {
  func didPressPost(_ viewController: PostsListViewController ,post: Post)
}

class PostsListViewController: UIViewController, Alertable, Refreshable, Emptiable {
  
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var viewModel: PostsListViewModel!
  var delegate: PostsListViewControllerDelegate?
  lazy var emptyView: EmptyView! = {
    let view = EmptyView(title: "No post to read.", image: #imageLiteral(resourceName: "post"))
    return view
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setUI()
    bind()
    setRefreshControl()
  }
  
  private func bind () {
    viewModel.onFetched = { [unowned self] in
      self.collectionView.reloadData()
      self.viewModel.posts.isEmpty ? self.setEmptyView(in: self.collectionView) : self.removeEmptyView()
      self.stopRefreshing()
    }
    
    viewModel.onError = { [unowned self] in
      let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
      self.showAlert(title: "Oops Someting went wrong...",
                     message: "Try to reload, or come back later.",
                     actions: alertAction)
      self.stopRefreshing()
    }
    viewModel.fetch()
  }

  private func setUI() {
    view.backgroundColor = Theme.Colors.lightGray
    collectionView.contentInset.top = 20
    collectionView.contentInset.bottom = 20
    navigationController?.navigationBar.prefersLargeTitles = true
  }
  
  internal func handleRefresControl() {
    viewModel.fetch()
  }
}

// Mark: - CollectionView Datasource and Delegate methods
extension PostsListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return viewModel.posts.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellIdentifier, for: indexPath) as! PostCollectionViewCell
    cell.configure(post: viewModel.posts[indexPath.item], delegate: self)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let post = viewModel.posts[indexPath.item]
    let height = post.title.height(withConstrainedWidth: self.collectionView.bounds.width - 40 - 40 - 28, font: Theme.Font.title) + 40
    return CGSize(width: collectionView.bounds.width - 40, height: height)
  }
}

// Mark: - PostCollectionViewCell delegate
extension PostsListViewController: PostCollectionViewCellDelegate {
  func didTapCell(_ cell: PostCollectionViewCell) {
    guard let indexPath = collectionView.indexPath(for: cell) else {
      return
    }
    
    let post = viewModel.posts[indexPath.item]
    delegate?.didPressPost(self, post: post)
  }
}

