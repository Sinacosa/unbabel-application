//
//  Coordinator.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 24/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import UIKit

fileprivate let postListViewControllerIdentifier = "postListViewControllerIdentifier"
fileprivate let postDetailIdentidier = "postDetailIdentidier"

struct AppCoordinator {
  
  let rootViewController: UINavigationController!
  init(navigationController: UINavigationController) {
    rootViewController = navigationController
    rootViewController.navigationBar.setTheme()
  }
  
  func start () {
    showPostList()
  }
  
  func showPostList() {
    guard let postListViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: postListViewControllerIdentifier) as? PostsListViewController else { return }
    postListViewController.viewModel = PostsListViewModel(api: NetworkLayer.self)
    postListViewController.delegate = self
    rootViewController.pushViewController(postListViewController, animated: true)
  }
}

extension AppCoordinator: PostsListViewControllerDelegate {

  func didPressPost(_ viewController: PostsListViewController, post: Post) {
    guard let postDetailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: postDetailIdentidier) as? PostDetailViewController else { return }
    postDetailViewController.viewModel = PostDetailViewModel(post: post, api: NetworkLayer.self)
    rootViewController.pushViewController(postDetailViewController, animated: true)
  }
}


