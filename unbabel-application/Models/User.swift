//
//  User.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class User: Object, OfflineStorable {
  @objc dynamic var id = 0
  @objc dynamic var name: String!
  @objc dynamic var username: String!
  @objc dynamic var email: String!
  
  convenience init(json: JSON) {
    self.init()
    self.id = json["id"].intValue
    self.name = json["name"].stringValue
    self.username = json["username"].stringValue
    self.email = json["email"].string
  }
  
  override static func primaryKey() -> String? {
    return "id"
  }
}
