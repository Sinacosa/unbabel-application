//
//  Post.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Post: Object, OfflineStorable {
  @objc dynamic var id = 0
  @objc dynamic var title = ""
  @objc dynamic var body = ""
  @objc dynamic var userId = 0
  
  convenience init(json: JSON) {
    self.init()
    self.id = json["id"].intValue
    self.title = json["title"].stringValue
    self.body = json["body"].stringValue
    self.userId = json["userId"].intValue
  }
  
  override static func primaryKey() -> String? {
    return "id"
  }
}

