//
//  OfflineStorable.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 24/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import RealmSwift

protocol OfflineStorable {
  
  associatedtype SelfType
  
  func saveOffline(update: Bool)
  func updateOffline(setProperties: () -> Void)
  
  static func fetchAllOffline() -> [SelfType]
  static func fetchOffline(filter: String, sortedBy sort: Sort?) -> [SelfType]
  
  func deleteOffline()
}

struct Sort {
  var keyPathProperty: String
  var ascending: Bool
}

extension OfflineStorable where Self: Object {
  
  func saveOffline(update: Bool = true) {
    let realm = try! Realm()
    try! realm.write {
      realm.add(self, update: update)
    }
  }
  
  static func find(primarykey: Any) -> Self? {
    let realm = try! Realm()
    return realm.object(ofType: Self.self, forPrimaryKey: primarykey)
  }
  
  func updateOffline(setProperties: () -> Void) {
    let realm = try! Realm()
    try! realm.write {
      setProperties()
    }
  }
  
  func deleteOffline() {
    let realm = try! Realm()
    try! realm.write {
      realm.delete(self)
    }
  }
  
  static func fetchAllOffline() -> [Self] {
    let realm = try! Realm()
    let objects = Array(realm.objects(self.self))
    return objects
  }
  
  static func fetchOffline(filter: String, sortedBy sort: Sort? = nil) -> [Self] {
    let realm = try! Realm()
    let objects = realm.objects(self.self).filter(filter)
    var results = objects
    if let sort = sort {
      results = objects.sorted(byKeyPath: sort.keyPathProperty, ascending: sort.ascending)
    }
    return Array(results)
  }
}

extension Array where Element: OfflineStorable, Element: Object {
  func saveOffline(update: Bool = true) {
    let realm = try! Realm()
    try! realm.write {
      realm.add(self, update: update)
    }
  }
  
  func deleteOffline() {
    let realm = try! Realm()
    try! realm.write {
      realm.delete(self)
    }
  }
}

