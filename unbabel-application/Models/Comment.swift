//
//  Comment.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Comment: Object, OfflineStorable {
  
  @objc dynamic var id = 0
  @objc dynamic var name = ""
  @objc dynamic var body = ""
  @objc dynamic var email = ""
  @objc dynamic var postId = 0
  
  convenience init (json: JSON) {
    self.init()
    self.id  = json["id"].intValue
    self.name = json["name"].stringValue
    self.body = json["body"].stringValue
    self.email = json["email"].stringValue
    self.postId = json["postId"].intValue
  }
  
  override static func primaryKey() -> String? {
    return "id"
  }
}
