//
//  postsListViewModel.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation

final class PostsListViewModel {
  
  var posts: [Post] = []
  var api: Requestable.Type
  var onFetched: (() -> Void)?
  var onError: (() -> Void)?
  
  init(api: Requestable.Type) {
    self.api = api
  }
  
  func fetch () {
    posts = Post.fetchAllOffline().sorted { $0.id < $1.id }
    api.getAllPosts { [weak self] (success, posts) in
      if success, let posts = posts {
        self?.posts = posts.sorted { $0.id < $1.id }
        self?.posts.saveOffline()
      } else {
        self?.onError?()
      }
      self?.onFetched?()
    }
  }
}
