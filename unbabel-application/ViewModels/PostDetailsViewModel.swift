//
//  PostDetailsViewModel.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import Foundation
import RealmSwift

final class PostDetailViewModel {
  
  var post: Post
  var comments: [Comment] = []
  var user: User?
  var api: Requestable.Type
  var onFetched: (() -> Void)?
  var onError: (() -> Void)?
  
  init(post: Post, api: Requestable.Type) {
    self.post = post
    self.api = api
  }
  
  func fetch () {
    self.comments = Comment.fetchOffline(filter: "postId = \(post.id)")
    self.user = User.find(primarykey: post.userId)
    api.getUserAndComments(for: post) { [weak self] (success, user, comments) in
      if success {
        self?.user = user
        self?.user?.saveOffline()
        if let comments = comments {
          self?.comments = comments
          self?.comments.saveOffline()
        }
      } else {
        self?.onError?()
      }
      self?.onFetched?()
    }
  }
}
