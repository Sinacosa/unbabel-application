//
//  PostDetailCollectionViewCell.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

class PostDetailCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var postTitleLabel: UILabel!
  @IBOutlet weak var postBodyLabel: UILabel!
  @IBOutlet weak var userNameLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setUI()
  }
  
  private func setUI() {
    backgroundColor = Theme.Colors.darkBlue
    postBodyLabel.numberOfLines = 0
    postTitleLabel.numberOfLines = 0
    layer.cornerRadius = 6
    postTitleLabel.font = Theme.Font.title
    postBodyLabel.font = Theme.Font.medium
    userNameLabel.font = Theme.Font.small
    [postTitleLabel, postBodyLabel, userNameLabel].forEach { $0?.textColor = .white}
  }
  
  func configure (post: Post, user: User?) {
    postTitleLabel.text = post.title.capitalizingFirstLetter()
    postBodyLabel.text = post.body
    userNameLabel.text = user?.name ?? ""
  }
  
  static func getHeight(width: CGFloat, post: Post) -> CGSize {
    let titleHeight = post.title.height(withConstrainedWidth: width - 40, font: Theme.Font.title)
    let bodyHeight = post.body.height(withConstrainedWidth: width - 40, font: Theme.Font.medium)
    let finalHeight = titleHeight + bodyHeight + 90
    return CGSize(width: width, height: finalHeight)
  }
}
