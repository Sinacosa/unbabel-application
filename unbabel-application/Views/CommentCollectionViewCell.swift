//
//  CommentCollectionViewCell.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

class CommentCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var commentLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setUI()
  }
  
  private func setUI() {
    backgroundColor = .white
    layer.cornerRadius = 6
    [commentLabel, emailLabel].forEach {
      $0?.font = Theme.Font.medium
      $0?.textColor = Theme.Colors.darkBlue
      $0?.numberOfLines = 0
    }
  }
  
  func configure(comment: Comment) {
    commentLabel.text = comment.body
    emailLabel.text = comment.email
  }
  
  static func getHeight(width: CGFloat,comment: Comment) -> CGSize {
    let bodyHeight = comment.body.height(withConstrainedWidth: width - 40, font: Theme.Font.medium)
    let emailHeight = comment.email.height(withConstrainedWidth: width - 40, font: Theme.Font.medium)
    return CGSize(width: width, height: emailHeight + bodyHeight + 60)
  }
}
