//
//  PostCollectionViewCell.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

protocol PostCollectionViewCellDelegate: class where Self: UIViewController {
  func didTapCell(_ cell: PostCollectionViewCell)
}

class PostCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var postTitleLabel: UILabel!
  @IBOutlet weak var imageView: UIImageView!
  weak var delegate: PostCollectionViewCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    setUI()
    setGestures()
  }
  
  private func setGestures() {
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture(gesture:)))
    addGestureRecognizer(tapGesture)
  }
  
  @objc private func handleTapGesture(gesture: UITapGestureRecognizer) {
    delegate?.didTapCell(self)
  }
  
  private func setUI() {
    layer.cornerRadius = 6
    layer.masksToBounds = true
    postTitleLabel.numberOfLines = 0
    postTitleLabel.font = Theme.Font.title
    postTitleLabel.textColor = Theme.Colors.darkBlue
    imageView.image = #imageLiteral(resourceName: "arrow")
  }
  
  func configure (post: Post, delegate: PostCollectionViewCellDelegate) {
    postTitleLabel.text = post.title.capitalizingFirstLetter()
    self.delegate = delegate
  }
}
