//
//  DetailTitleView.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 23/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

class DetailTitleView: UICollectionReusableView {
  
  var titleLabel: UILabel = {
    let label = UILabel()
    label.font = Theme.Font.medium
    label.textColor = Theme.Colors.darkBlue
    label.textAlignment = .center
    return label
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUI()
  }
  
  private func setUI() {
    addSubview(titleLabel)
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 1).isActive = true
    titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 1).isActive = true
    titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 1).isActive = true
    titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 1).isActive = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
