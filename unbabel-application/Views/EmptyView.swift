//
//  EmptyView.swift
//  unbabel-application
//
//  Created by Mathieu Giquel on 25/01/2018.
//  Copyright © 2018 Mathieu Giquel. All rights reserved.
//

import UIKit

class EmptyView: UIView {
  private var titleLabel = UILabel()
  private var imageView = UIImageView()
  private var image: UIImage!
  
  convenience init(title: String, image: UIImage) {
    self.init(frame: .zero)
    self.titleLabel.text = title
    self.imageView.image = image
    self.image = image
    setUI()
  }
  
  private func setUI() {
    ([titleLabel, imageView] as [UIView]).forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      addSubview($0)
    }
    imageView.widthAnchor.constraint(equalToConstant: image.size.width).isActive = true
    imageView.heightAnchor.constraint(equalToConstant: image.size.height).isActive = true
    imageView.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
    imageView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -image.size.height).isActive = true
    
    titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
    titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
    titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
    titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: 20)
    titleLabel.numberOfLines = 0
    titleLabel.textAlignment = .center
    titleLabel.textColor = Theme.Colors.darkBlue
    titleLabel.font = Theme.Font.largeTitle
  }
}
