# Unbabel Application
#### Installation

The app is using `cocoapods` to manage the third party librairies.
To install the app you should run: 

```
$ pod install 
```

and open the `unbabel-application.xcworkspace` file.

#### Pods:
- Alamofire
- SwiftyJSON
- Realm

#### Architecture
The application architecture is MVVC and Coordinator.
I tried to decouple the view-models, view-controllers and the network layer.

#### Persistence
The persistence is managed by Realm, with the models:
- Post
- User
- Comment

#### Features:

The app first loads the posts and saves them in the persistence layer (realm). When the post detail screen is shown, the app will synchronise the user request and the comments request, and save the objects in the persistence layer.

It means, if the user never visited the post detail screen for a specific post, the app is not be able to display the name of the user and the comments in the post detail screen when the network is off.

#### Screenshots

![Screenshot](/screenshots/app-screen.png "Screenshot Post List")

